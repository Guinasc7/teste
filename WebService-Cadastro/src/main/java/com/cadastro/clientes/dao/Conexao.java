/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cadastro.clientes.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 */
public class Conexao {

    private Connection con;
    private final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private final String USER = "root";
    private final String PASSWORD = "root4321";
    private final String DBURL = "jdbc:mysql://localhost:3306/cliente?serverTimezone=UTC";

    private static Conexao conexao;

    public Conexao() {
    }

    public static Conexao getInstance() throws SQLException {
        if (conexao == null) {
            conexao = new Conexao();
        }
        conexao.connect();
        return conexao;
    }

    private void connect() throws SQLException {
        if (con == null || con.isClosed()) {
            try {
                Class.forName(DRIVER);
            } catch (ClassNotFoundException e) {
                Logger.getLogger(Conexao.class.getName()).log(Level.SEVERE, null, e);
            }
            con = DriverManager.getConnection(DBURL, USER, PASSWORD);
        }
    }

    public boolean ExisteConexao() throws SQLException {
        return con != null && !con.isClosed();
    }

    public Connection getCon() {
        return con;
    }
}
