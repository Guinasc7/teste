/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cadastro.clientes.dao;

import com.cadastro.clientes.entidades.Clientes;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ClientesDao implements IDAO<Clientes> {

    private PreparedStatement insert, delete, update, search, list;

    private static ClientesDao usuariosDao;

    private ClientesDao() {

    }

    public static ClientesDao getInstance() {
        if (usuariosDao == null) {
            usuariosDao = new ClientesDao();
        }
        return usuariosDao;
    }

    @Override
    public void insert(Clientes entity) throws SQLException {
        String query = "INSERT INTO cliente (nome, cpf) VALUES (?, ?)";

        if (insert == null) {
            insert = Conexao.getInstance().getCon().prepareStatement(query);
        }

        insert.setString(1, entity.getNome());
        insert.setString(2, entity.getCpf());
        insert.executeUpdate();
    }

    @Override
    public void delete(String cpf) throws SQLException {
        String query = "DELETE FROM cliente WHERE cpf = ?";

        if (delete == null) {
            delete = Conexao.getInstance().getCon().prepareStatement(query);
        }

        delete.setString(1, cpf);
        delete.executeUpdate();
    }

    @Override
    public List<Clientes> list() throws SQLException {
        String query = "SELECT * FROM cliente";

        if (list == null) {
            list = Conexao.getInstance().getCon().prepareStatement(query);
        }

        ResultSet resultSet = list.executeQuery();
        ArrayList<Clientes> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(load(resultSet));
        }

        return result;
    }

    @Override
    public Clientes search(String cpf) throws SQLException {
        String query = "SELECT * FROM cliente WHERE cpf = ?";

        if (search == null) {
            search = Conexao.getInstance().getCon().prepareStatement(query);
        }

        search.setString(1, cpf);

        ResultSet resultSet = search.executeQuery();

        return resultSet != null && resultSet.next() ? load(resultSet) : null;
    }
   

    @Override
    public void update(Clientes entity) throws SQLException {
        String query = "UPDATE cliente SET nome = ?, cpf = ? WHERE id = ?";

        if (update == null) {
            update = Conexao.getInstance().getCon().prepareStatement(query);                    
        }
        
        update.setString(1, entity.getNome());
        update.setString(2, entity.getCpf());
        update.setInt(4, entity.getId());
        update.executeUpdate();
    }

    public Clientes load(ResultSet resultSet) throws SQLException {
        Clientes user = new Clientes();

        user.setId(resultSet.getInt("id"));
        user.setNome(resultSet.getString("nome"));
        user.setCpf(resultSet.getString("cpf"));

        return user;
    }

}
