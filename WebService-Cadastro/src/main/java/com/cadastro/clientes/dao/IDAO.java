/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cadastro.clientes.dao;

import java.sql.SQLException;
import java.util.List;

/**
 *
 */
public interface IDAO<T> {

    void insert(T entity) throws SQLException;
    
    void delete(String cpf) throws SQLException;

    List<T> list() throws SQLException;
    
    T search(String cpf) throws SQLException;
        
    void update(T entity) throws SQLException;
}
