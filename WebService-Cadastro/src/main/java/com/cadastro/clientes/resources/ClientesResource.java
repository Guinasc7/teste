/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cadastro.clientes.resources;

import com.cadastro.clientes.dao.ClientesDao;
import com.cadastro.clientes.entidades.Clientes;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 */
@Path("clientes")
public class ClientesResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Clientes> get() {
        try {
            return ClientesDao.getInstance().list();
        } catch (SQLException e) {
            Logger.getLogger(ClientesResource.class.getName()
                    + "Erro ao consultar o banco de dados: "
                    + e.getMessage())
                    .log(Level.SEVERE, null, e);
        }

        return null;
    }
    

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void post(Clientes usuarios) {
        try {
            ClientesDao.getInstance().insert(usuarios);
        } catch (SQLException e) {
            Logger.getLogger(ClientesResource.class.getName())
                    .log(Level.SEVERE, null, e);
        }
    }

    @GET
    @Path("{cpf}")
    @Produces(MediaType.APPLICATION_JSON)
    public Clientes getUserByName(@PathParam("cpf")String cpf) {
        try {
            return ClientesDao.getInstance().search(cpf);
        } catch (SQLException e) {
            Logger.getLogger(ClientesResource.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }
    
    @DELETE
    @Path("{cpf}")
    public void delete(@PathParam("cpf")String cpf){
        try{
            ClientesDao.getInstance().delete(cpf);           
        }catch(SQLException e){
            Logger.getLogger(ClientesResource.class.getName()).log(Level.SEVERE, null, e);
        }
    }
    
    @PUT
    @Path("{id}")
    public void put(@PathParam("id")Integer id, Clientes usuarios){
        usuarios.setId(id);
        
        try{
            ClientesDao.getInstance().update(usuarios);
        }catch(SQLException e){
            Logger.getLogger(ClientesResource.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
