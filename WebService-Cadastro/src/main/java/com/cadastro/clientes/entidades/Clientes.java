/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cadastro.clientes.entidades;

/**
 *
 */
public class Clientes {

    private int id;
    private String nome, cpf;

    public Clientes() {
    }

    public Clientes(String Nome, String Cpf) {
        this.nome = Nome;
        this.cpf = Cpf;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
}
