import { Cliente } from './cliente';

export const CLIENTES: Cliente[] = [
  { id: 2, nome: 'Isabela', cpf: '400.690.821-79' },
  { id: 3, nome: 'Maria', cpf: '828.815.431-28' },
  { id: 4, nome: 'Sileni', cpf: '044.084.788-57' }
];