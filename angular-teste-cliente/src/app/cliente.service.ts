import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Cliente } from './cliente';
import { MensagemService } from './mensagem.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class ClienteService {

  private ClientesUrl = 'http://192.168.1.175:8084/cadastro/webapi/clientes'; 

  constructor(
    private http: HttpClient,
    private mensagemService: MensagemService) { }

  getClientes (): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.ClientesUrl)
      .pipe(
        tap(clientes => this.log('busca de clientes')),
        catchError(this.handleError('getClientes', []))
      );
  }

  getClienteNo404<Data>(id: number): Observable<Cliente> {
    const url = `${this.ClientesUrl}/?id=${id}`;
    return this.http.get<Cliente[]>(url)
      .pipe(
        map(clientes => clientes[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `encontrado` : `não encontrado`;
          this.log(`${outcome} cliente id=${id}`);
        }),
        catchError(this.handleError<Cliente>(`getCliente id=${id}`))
      );
  }

  getCliente(id: number): Observable<Cliente> {
    const url = `${this.ClientesUrl}/${id}`;
    return this.http.get<Cliente>(url).pipe(
      tap(_ => this.log(`cliente encontrado id=${id}`)),
      catchError(this.handleError<Cliente>(`getCliente id=${id}`))
    );
  }

  searchClientes(term: string): Observable<Cliente[]> {
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Cliente[]>(`${this.ClientesUrl}/?cpf=${term}`).pipe(
      tap(_ => this.log(`cliente encontrado "${term}"`)),
      catchError(this.handleError<Cliente[]>('buscaClientes', []))
    );
  }


  addCliente (cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.ClientesUrl, cliente, httpOptions).pipe(
      tap((cliente: Cliente) => this.log(`cliente adicionado w/ id=${cliente.id}`)),
      catchError(this.handleError<Cliente>('addCliente'))
    );
  }

  deleteCliente (cliente: Cliente | number): Observable<Cliente> {
    const id = typeof cliente === 'number' ? cliente : cliente.id;
    const url = `${this.ClientesUrl}/${id}`;

    return this.http.delete<Cliente>(url, httpOptions).pipe(
      tap(_ => this.log(`cliente deletado id=${id}`)),
      catchError(this.handleError<Cliente>('deleteCliente'))
    );
  }

  updateCliente (cliente: Cliente): Observable<any> {
    return this.http.put(this.ClientesUrl, cliente, httpOptions).pipe(
      tap(_ => this.log(`cliente atualizado id=${cliente.id}`)),
      catchError(this.handleError<any>('updateCliente'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    this.mensagemService.add(`ClienteService: ${message}`);
  }
}