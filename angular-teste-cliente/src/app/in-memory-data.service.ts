import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Cliente } from './cliente';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const clientes = [
        { id: 2, nome: 'Isabela', cpf: '400.690.821-79' },
        { id: 3, nome: 'Maria', cpf: '828.815.431-28' },
        { id: 4, nome: 'Sileni', cpf: '044.084.788-57' }
    ];
    return {clientes};
  }

  genId(clientes: Cliente[]): number {
    return clientes.length > 0 ? Math.max(...clientes.map(cliente => cliente.id)) + 1 : 11;
  }
}