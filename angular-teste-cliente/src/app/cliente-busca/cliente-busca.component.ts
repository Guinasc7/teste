import { Component, OnInit } from '@angular/core';

import { Observable, Subject } from 'rxjs';

import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { Cliente } from '../cliente';
import { ClienteService } from '../cliente.service';

@Component({
  selector: 'app-cliente-busca',
  templateUrl: './cliente-busca.component.html',
  styleUrls: [ './cliente-busca.component.css' ]
})
export class ClienteBuscaComponent implements OnInit {
  clientes$: Observable<Cliente[]>;
  private searchTerms = new Subject<string>();

  constructor(private clienteService: ClienteService) {}

  search(term: string): void {
    this.searchTerms.next(term);
  }

  ngOnInit(): void {
    this.clientes$ = this.searchTerms.pipe(
      debounceTime(300),

      distinctUntilChanged(),

      switchMap((term: string) => this.clienteService.getClientes(term)),
    );
  }
}
