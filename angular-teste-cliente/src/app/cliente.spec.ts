import { TestBed } from '@angular/core/testing';

import { Cliente } from './cliente';

describe('ClienteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Cliente = TestBed.get(Cliente);
    expect(service).toBeTruthy();
  });
});
