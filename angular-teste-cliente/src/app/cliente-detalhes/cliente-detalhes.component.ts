import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Cliente }         from '../cliente';
import { ClienteService }  from '../cliente.service';

@Component({
  selector: 'app-cliente-detalhes',
  templateUrl: './cliente-detalhes.component.html',
  styleUrls: [ './cliente-detalhes.component.css' ]
})
export class ClienteDetalhesComponent implements OnInit {
  @Input() cliente: Cliente;

  constructor(
    private route: ActivatedRoute,
    private clienteService: ClienteService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getCliente();
  }

  getCliente(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.clienteService.getCliente(id)
      .subscribe(cliente => this.cliente = cliente);
  }

  voltar(): void {
    this.location.back();
  }

 salvar(): void {
    this.clienteService.updateCliente(this.cliente)
      .subscribe(() => this.voltar());
  }
}
