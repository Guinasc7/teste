import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent }         from './app.component';
import { DashboardComponent }   from './dashboard/dashboard.component';
import { ClienteDetalhesComponent }  from './cliente-detalhes/cliente-detalhes.component';
import { ClientesComponent }      from './clientes/clientes.component';
import { ClienteBuscaComponent }  from './cliente-busca/cliente-busca.component';
import { MensagensComponent }    from './mensagens/mensagens.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    ClientesComponent,
    ClienteDetalhesComponent,
    MensagensComponent,
    ClienteBuscaComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
